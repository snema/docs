Обновление ПО
=============

1. Установка защитного ключа-перемычки (Boot_key):
  
  Для снятия ограничений на изменение ПО и калибровочных коэффициентов необходимо установить ключ-перемычку, замыкающую контакты DAC1 и GND мезонинного разъема, расположенного с обратной стороны платы контроллера.
  Если в контроллере имеется модуль беспроводной связи, перемычку необходимо установить на нем. Для доступа к мезонинному разъему необходимо разобрать контроллер согласно разделу :ref:`Техническое обслуживание и ремонт`. 
  
  Далее подать питание на контроллер и подключиться к контроллеру по одному из интерфейсов Ethernet или USB.

  После завершения обновления ПО необходимо убрать перемычку во избежание непреднамеренного изменения ПО контроллера.

.. note::
  В контроллере может одновременно находиться 2 версии ПО: OS1 и OS2.
  Так же имеется понятие Main OS (главное ПО – то ПО, которое будет запущено в случае сброса питания или перезапуска) и
  Current OS (текущее ПО – ПО, которое исполняется в настоящий момент) Загрузка, смена и откат ПО производится через WEB-интерфейс контроллера.
  По умолчанию при подключении через интерфейс Ethernet IP-адрес: 192.168.1.232, при подключении через интерфейс USB IP-адрес: 172.16.2.232


2. Загрузка новой версии ПО:

  На главной WEB-странице контроллера введите пароль доступа (по умолчанию "bric"), нажмите на кнопку "Download OS" и выберите запрашиваемый файл. После нажатия кнопки «Download» дождитесь окончания загрузки.

3. Запуск новой версии ПО:

  В случае успешной загрузки откроется панель управления ПО (Operation System Control Panel). Имеется 2 варианта запуска:

  "Safe start OS" – запуск нового ПО в безопасном режиме. В этом режиме контроллер запускается с новым ПО и работает в течение 10 минут. Если в течение этого времени не подтвердить работоспособность текущей версии ПО, произойдет автоматический откат на ранее установленную версию. Данный режим позволяет проверить работоспособность контроллера после обновления и в случае возникновения каких-либо проблем вернуться к предыдущему рабочему варианту.

  "Set main and start OS" – запуск нового ПО в нормальном режиме. В этом режиме контроллер запускается с новым ПО и через 2 сек автоматически подтверждает работоспособность текущей версии. Это сделано для того, чтобы в случае неудачной загрузки произошел автоматический откат на ранее установленную стабильную версию.

4. Подтверждение текущей версии ПО

  Для подтверждения текущей версии ПО зайдите на главную WEB-страницу контроллера. Далее нажмите кнопку "OS control". В выпадающем списке выберите команду "Set current OS as main" и отправьте команду нажатием кнопки «Send command».

  .. note::
    команда «Set current OS as main» не будет отображаться в списке команд, если текущее ПО работает в нормальном режиме.
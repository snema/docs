Утилизация
==========

1.	Контроллер и материалы, используемые при изготовлении, не представляют опасности для жизни, здоровья людей и окружающей среды, как в процессе эксплуатации, так и после окончания срока эксплуатации и подлежат утилизации.
   
2.	Конструкция модуля не содержит химически и радиационно-опасных компонентов.
   
3.	По истечении срока службы модуль утилизируется путем разборки.
   
4.	При утилизации отходов материалов, а также при обустройстве приточно-вытяжной вентиляции рабочих помещений должны соблюдаться требования по охране природы согласно ГОСТ 17.1.1.01, ГОСТ 17.1.3.13, ГОСТ 17.2.3.02 и ГОСТ 17.2.1.04. 
   
5.	Утилизация отходов материалов – согласно СанПиН 2.1.7.1322.
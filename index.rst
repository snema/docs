.. bric_plc documentation master file, created by
   sphinx-quickstart on Sun Jun 14 16:52:51 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. only:: html

   .. hint::
      Ссылка для скачивания печатной версии технической документации: |pdf|

.. |pdf| image:: images/pdf.png
    :target: https://bric-plc.readthedocs.io/_/downloads/ru/latest/pdf/


.. only:: html
   
   Программируемый логический контроллер BRIC
   ==========================================
   **Универсальный промышленный контроллер BRIC** соответствует ТУ 27.33.13.161-001-00 354407-2018 и предназначен для построения локальных и территориально-распределенных систем автоматики технологических объектов малого и среднего уровня сложности.

   .. raw:: html
       
       <iframe id="b3593afc-e47b-45db-be35-10a05b53cb72" src="https://app.vectary.com/viewer/v1/?model=b3593afc-e47b-45db-be35-10a05b53cb72&env=studio1" frameborder="0" width="100%" height="480"></iframe>

.. only:: html

   .. hint::
      
      Скачать 3D-модель в формате ``.stp`` можно здесь_

.. _здесь: https://disk.yandex.ru/d/H-GordG1X579ag

.. toctree::
   :maxdepth: 2
   :caption: Содержание
   :hidden:

   main_options
   specifications
   exterior
   interior
   configurations
   completeness
   special_operating_modes
   digital_inputs
   digital_outputs
   analog_inputs
   communication_interfaces
   intermodular_connection
   safety_precautions
   installation
   software_update
   maintenance_and_repair
   marking
   pack
   resources_shelf_life
   transportation
   disposal
   testing
   address_space_bric

.. toctree::
   :maxdepth: 1
   :caption: Версия платы
   :hidden:

   V2 <https://bric-plc.readthedocs.io/ru/v2/>

.. toctree::
   :maxdepth: 1
   :caption: Линейка BRIC
   :hidden:

   Модуль расширения аналоговых выходов BRIC-AO-4 <https://bric-ao.readthedocs.io/ru/latest/index.html>
   Модуль расширения аналоговых входов BRIC-AI-16 <https://bric-ai.readthedocs.io/ru/latest/index.html>
   Модуль расширения дискретных входов BRIC-DI-16 <https://bric-di.readthedocs.io/ru/latest/index.html>
   Модуль расширения дискретных выходов BRIC-DO-8 <https://bric-do.readthedocs.io/ru/latest/index.html>
   Модуль расширения дискретных выходов BRIC-DO-16 <https://bric-do16.readthedocs.io/ru/latest/index.html>

.. toctree::
   :maxdepth: 1
   :caption: HART/RS-485
   :hidden:

   Интерфейсный преобразователь «USB-HART/RS-485» <https://bric-usb_hart.readthedocs.io/ru/latest/index.html>


.. toctree::
   :maxdepth: 1
   :caption: Beremiz/BRIC IDE
   :hidden:

   Руководство по программированию ПЛК BRIC в среде разработки Beremiz <https://bric-beremiz.readthedocs.io/ru/latest/index.html>
   Обучающие уроки по программированию в среде Beremiz <https://brz-des.readthedocs.io/ru/stable/>
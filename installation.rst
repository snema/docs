Монтаж
======

Контроллер устанавливается на DIN-рейку типа ТН-35, профиль которой изображен на рисунке:

.. figure:: images/din.png
   :width: 300

Монтаж контроллера на DIN-рейку осуществляется с помощью клипсы, расположенной на задней стенке корпуса.

Для установки контроллера необходимо сначала надавить на верхний подпружиненный выступ клипсы, после чего защелкнуть нижний выступ.

Для снятия контроллера  необходимо сначала надавить на верхний подпружиненный выступ клипсы, после чего потянуть нижнюю часть корпуса на себя.

.. figure:: images/din_install.png
  :width: 250

.. note::
  Для заземления корпуса в нижних углах корпуса расположены контакты.